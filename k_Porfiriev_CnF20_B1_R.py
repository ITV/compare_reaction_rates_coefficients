#!/usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np 

NA = 6.02214076e23
# in cal/mol/K
R_cal = 1.9872159

T = np.linspace(1000.0/500.0, 1000.0/ 3000.0, 200)
k_table1 = np.empty(len(T))

for i in range(len(T)):
  temp = 1000.0/T[i]
  k_table1[i] = (4.17E+173)*np.power(temp, -38.025)* np.exp(-1.9347E+05/R_cal/temp)

T_plot_B1_R_0_01atm = np.array([
5.0023641348e-1,
5.2011070427e-1,
5.4583303117e-1,
5.6395696563e-1,
5.7740438308e-1,
5.8559051287e-1,
5.9816242252e-1,
6.1687409433e-1,
6.3412383952e-1,
6.4845335745e-1,
6.6277957904e-1,
6.7506437751e-1,
6.9436576448e-1,
7.1366781072e-1 ])

k_data_B1_R_0_01atm = np.array([
 1.4859955363e+3,
 9.4130596405e+2,
 4.7944809492e+2,
 2.8532199863e+2,
 1.9032939430e+2,
 1.4530220552e+2,
 9.4928727338e+1,
 5.0389556154e+1,
 2.8172823234e+1,
 1.5586650581e+1,
 9.5680160715e+0,
 5.3483777459e+0,
 2.3791737599e+0,
 1.0365754434e+0 ])

plt.yscale("log")
plt.xlabel("1000/$T$ [1/K]") 
plt.ylabel("$k$ [1/s]") 
plt.title("B1->R rate coefficient, Porfiriev et al., CnF 213 (2020) 302-313")
plt.xlim([0.35, 1.0])
plt.ylim([1.0, 10e8])
plt.grid()

print("DIVIDE TABLE RATE COEFF. BY NA (INCORRECT?)")
k_table1 = k_table1/NA

plt.plot(T, k_table1, label='Table 1 rate coeff. * 1/NA', color='black', marker="x")
plt.plot(T_plot_B1_R_0_01atm, k_data_B1_R_0_01atm, label='B1=>R, 0.01atm (from Fig. 3b))', color='cyan')
plt.legend()

plt.savefig("k_B1_R_0_01atm" + "." + "pdf", format="pdf", dpi=1200)
plt.show()

