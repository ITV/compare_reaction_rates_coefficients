#!/usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np 
import cantera as ct
glarborg_gas = ct.Solution('glarborg.yaml')

NA = 6.02214076e23
# in cal/mol/K
R_cal = 1.9872159
over_mol_to_over_kmole = 1.e-3
cm3_to_m3 = 1.0e6
conversion_bimolecular_k = over_mol_to_over_kmole * cm3_to_m3

T = np.linspace(1000.0/500.0, 1000.0/ 3000.0, 200)
k_bahng = np.empty(len(T))
k_bahng_upper = np.empty(len(T))
k_bahng_lower = np.empty(len(T))
k_glarborg = np.empty(len(T))
k_baulch = np.empty(len(T))

for i in range(len(T)):
  temp = 1000.0/T[i]
  glarborg_gas.TP = temp, 1.0e5
  # Bahng and Macdonald specify NH2+H=NH+H2
  # J. Phys. Chem. A 2009, 113, 11, 2415–2423
  # https://doi.org/10.1021/jp809643u
  k_bahng[i] = 7.7e-15 * NA 
  # Bahng and Macdonald always write (7.7+/-14)e-15 cm^3/mol/s
  # I'm not sure what -14 should mean
  k_bahng_lower[i] = (7.7-1.4)*1.0e-15 * NA 
  k_bahng_upper[i] = (7.7+1.4)*1.0e-15 * NA 
  # Peter Glarborg, James A. Miller, Branko Ruscic, Stephen J. Klippenstein,
  # Modeling nitrogen chemistry in combustion,
  # Progress in Energy and Combustion Science,
  # Volume 67,
  # 2018,
  # Pages 31-68,
  # https://doi.org/10.1016/j.pecs.2018.01.002
  # Glarborg et al. specify NH+H2=NH2+H -> use the reverse for the figure
  k_glarborg[i] = glarborg_gas.reverse_rate_constants[0] * conversion_bimolecular_k
  # Journal of Physical and Chemical Reference Data 34, 757 (2005); https://doi.org/10.1063/1.1748524
  # Baulch et al. specify NH2+H=NH+H2
  k_baulch[i] = 8.8e-11 * NA * np.exp(-2515.0/temp)

plt.plot(T,k_bahng, label='Bahng and Macdonald (2009), $k = 7.7 \\times 10^{{-15}}$ cm$^3$/molecule/s', color='green')
# Shade the area between k_bahng_lower and k_bahng_upper
plt.fill_between(T, k_bahng_lower, k_bahng_upper,
                 facecolor="green", 
                 color='green',     
                 alpha=0.2)        

plt.plot(T,k_baulch, label='Baulch et al. (2005)', color='red', linestyle='dotted') 
plt.plot(T,k_glarborg, label='Glarborg model (2018), $\\Delta \\log k = \\pm 0.2$', linewidth=2, color='black', linestyle='dashed' ) 
# According to Baulch et al. this should be only 0.2
logk = 0.2

k_glarborg_upper = np.power(10.0, logk) * k_glarborg
k_glarborg_lower = 1.0/np.power(10.0, logk) * k_glarborg

# Shade the area between k_glarborg_lower and k_glarborg_upper
plt.fill_between(T, k_glarborg_lower, k_glarborg_upper,
                 facecolor="black", 
                 color='black',     
                 alpha=0.2)         

plt.title("NH$_2$+H = NH+H$_2$") 
plt.xlabel("1000/$T$ [1/K]") 
plt.ylabel("$k$ [cm$^3$/mol/s]") 
plt.yscale("log")
plt.ylim(1.0e9, 1.0e15)
plt.grid()
plt.legend()
suffix = "pdf"
plt.savefig("k_NH2_H=NH_H2" + "." + suffix, format=suffix, dpi=1200)
plt.show()
   
