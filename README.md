# Comparison of reaction rate coefficients

This script generates a figure that compares reaction 
rate coefficients from the literature. References 
are in the script.

```sh
ck2yaml --input=glarborg.mech --thermo=glarborg.therm --transport=glarborg.trans
./k_NH2_H=NH_H2.py.py
```
